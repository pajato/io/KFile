// SPDX-License-Identifier: LGPL-3.0

object Versions {
    const val KOTLIN = "1.3.71"
    const val KFILE = "0.3.3"
}
